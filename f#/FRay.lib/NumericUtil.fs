namespace FRay

open System

[<AutoOpen>]
module NumericUtilFn =
    let roundedEquals (a: double) (b: double) = Math.Round(a, 4) = Math.Round(b, 4)

    let float32Equals a b =
        if Single.IsNaN(a) && Single.IsNaN(b) then
            true
        else
            roundedEquals (double a) (double b)
