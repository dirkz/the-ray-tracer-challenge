namespace FRay

open System.Numerics

[<AutoOpen>]
module Matrix4x4Extensions =
    type Matrix4x4 with

        member a.MyEquals(b: Matrix4x4) =
            float32Equals a.M11 b.M11
            && float32Equals a.M12 b.M12
            && float32Equals a.M13 b.M13
            && float32Equals a.M14 b.M14
            && float32Equals a.M21 b.M21
            && float32Equals a.M22 b.M22
            && float32Equals a.M23 b.M23
            && float32Equals a.M24 b.M24
            && float32Equals a.M31 b.M31
            && float32Equals a.M32 b.M32
            && float32Equals a.M33 b.M33
            && float32Equals a.M34 b.M34
            && float32Equals a.M41 b.M41
            && float32Equals a.M42 b.M42
            && float32Equals a.M43 b.M43
            && float32Equals a.M44 b.M44

        member a.Transposed() = Matrix4x4.Transpose(a)
