namespace FRay

open System

[<CustomEquality; NoComparison>]
type Matrix2x2 =
    struct
        val M11: float32
        val M12: float32
        val M21: float32
        val M22: float32

        new(m11, m12, m21, m22) =
            { M11 = m11
              M12 = m12
              M21 = m21
              M22 = m22 }

        static member Identity = Matrix2x2(1f, 0f, 0f, 1f)

        member this.Item
            with get (row, col) =
                match (row, col) with
                | (0, 0) -> this.M11
                | (0, 1) -> this.M12
                | (1, 0) -> this.M21
                | (1, 1) -> this.M22
                | _ -> raise (IndexOutOfRangeException())

        override a.ToString() =
            sprintf "Matrix2x2 %f %f %f %f" a.M11 a.M12 a.M21 a.M22

        member a.SharedEquals(b: Matrix2x2) =
            float32Equals a.M11 b.M11
            && float32Equals a.M12 b.M12
            && float32Equals a.M21 b.M21
            && float32Equals a.M22 b.M22

        override a.GetHashCode() =
            [| a.M11; a.M12; a.M21; a.M22 |].GetHashCode()

        // Note: This gets called when FSCheck compares matrices in a property.
        override a.Equals(b) =
            match b with
            | :? Matrix2x2 as b -> a.SharedEquals(b)
            | _ -> false

        interface IEquatable<Matrix2x2> with
            member a.Equals(b) = a.SharedEquals(b)

        member a.Transposed() = Matrix2x2(a.M11, a.M21, a.M12, a.M22)

        member a.Determinant() = a.M11 * a.M22 - a.M12 * a.M21

    end
