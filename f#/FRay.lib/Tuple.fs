namespace FRay

open System.Numerics

[<AutoOpen>]
module TupleFn =
    let tuple x y z w = Vector4(x, y, z, w)

    let isPoint (v: Vector4) = v.W = 1f

    let isVector (v: Vector4) = v.W = 0f

    let point x y z = tuple x y z 1f

    let vector x y z = tuple x y z 0f

    let dot a b = Vector4.Dot(a, b)

    let normalize a = Vector4.Normalize(a)

    let cross (a: Vector4) (b: Vector4) =
        let v1 = Vector3(a.X, a.Y, a.Z)
        let v2 = Vector3(b.X, b.Y, b.Z)
        let c = Vector3.Cross(v1, v2)
        vector c.X c.Y c.Z

    let color r g b = tuple r g b 0f

    type Vector4 with

        member this.R = this.X
        member this.G = this.Y
        member this.B = this.Z
        member this.A = this.W
        member this.DiscardAlpha = color this.X this.Y this.Z
