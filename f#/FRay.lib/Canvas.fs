namespace FRay

open System.IO

type Canvas(w, h) =
    let pixels = Array2D.init w h (fun i j -> color 0f 0f 0f)

    override _.ToString() = sprintf "Canvas %dx%d" w h

    member _.Width = w

    member _.Height = h

    member _.Item
        with get (i, j) = pixels[i, j]

    member _.WritePixel(i, j, c) = pixels[i, j] <- c

    member _.WriteTo(writer: TextWriter) =
        writer.WriteLine("P3")
        writer.WriteLine(sprintf "%d %d" w h)
        writer.WriteLine("255")
        writer.Flush()

        let clamp i =
            if i < 0 then 0
            else if i > 255 then 255
            else i

        let toRGB f = 255f * f |> round |> int

        let writeValue v col =
            let i = v |> toRGB |> clamp

            let baseString = sprintf "%d" i

            if col = 0 then
                writer.Write(baseString)
                col + baseString.Length
            else if col + baseString.Length + 1 < 70 then
                writer.Write(' ')
                writer.Write(baseString)
                col + baseString.Length + 1
            else
                writer.WriteLine()
                writer.Write(baseString)
                baseString.Length

        for j in 0 .. h - 1 do
            let mutable col = 0

            for i in 0 .. w - 1 do
                let c = pixels[i, j]
                [| c.R; c.G; c.B |] |> Array.iter (fun v -> col <- writeValue v col)

            writer.WriteLine()

        writer.Flush()

    member this.ToPPM() =
        let s = new StringWriter()
        this.WriteTo(s)
        s.ToString()

[<AutoOpen>]
module CanvasFn =
    let canvas w h = Canvas(w, h)
