namespace FRay

open System

[<CustomEquality; NoComparison>]
type Matrix3x3 =
    struct
        val M11: float32
        val M12: float32
        val M13: float32
        val M21: float32
        val M22: float32
        val M23: float32
        val M31: float32
        val M32: float32
        val M33: float32

        new(m11, m12, m13, m21, m22, m23, m31, m32, m33) =
            { M11 = m11
              M12 = m12
              M13 = m13
              M21 = m21
              M22 = m22
              M23 = m23
              M31 = m31
              M32 = m32
              M33 = m33 }

        static member Identity = Matrix3x3(1f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 1f)

        member this.Item
            with get (row, col) =
                match (row, col) with
                | (0, 0) -> this.M11
                | (0, 1) -> this.M12
                | (0, 2) -> this.M13
                | (1, 0) -> this.M21
                | (1, 1) -> this.M22
                | (1, 2) -> this.M23
                | (2, 0) -> this.M31
                | (2, 1) -> this.M32
                | (2, 2) -> this.M33
                | _ -> raise (IndexOutOfRangeException())

        override a.ToString() =
            sprintf
                "Matrix3x3\n%s\n%s\n%s"
                (sprintf "%f %f %f" a.M11 a.M12 a.M13)
                (sprintf "%f %f %f" a.M21 a.M22 a.M23)
                (sprintf "%f %f %f" a.M31 a.M32 a.M33)

        member a.SharedEquals(b: Matrix3x3) =
            float32Equals a.M11 b.M11
            && float32Equals a.M12 b.M12
            && float32Equals a.M13 b.M13
            && float32Equals a.M21 b.M21
            && float32Equals a.M22 b.M22
            && float32Equals a.M23 b.M23
            && float32Equals a.M31 b.M31
            && float32Equals a.M32 b.M32
            && float32Equals a.M33 b.M33

        override a.GetHashCode() =
            [| a.M11; a.M12; a.M13; a.M21; a.M22; a.M23; a.M31; a.M32; a.M33 |]
                .GetHashCode()

        // Note: This gets called when FSCheck compares matrices in a property.
        override a.Equals(b) =
            match b with
            | :? Matrix3x3 as b -> a.SharedEquals(b)
            | _ -> false

        interface IEquatable<Matrix3x3> with
            member a.Equals(b) = a.SharedEquals(b)

        member a.Transposed() =
            Matrix3x3(a.M11, a.M21, a.M31, a.M12, a.M22, a.M32, a.M13, a.M23, a.M33)

    end
