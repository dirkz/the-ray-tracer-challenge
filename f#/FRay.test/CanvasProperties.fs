module FRay.Test.CanvasProperties

open FRay

open FsCheck.Xunit
open FsCheck
open System.Numerics

type ArbColor(r, g, b) =
    member _.ToVector() = Vector4(r, g, b, 1f)
    override _.ToString() = sprintf "%f %f %f" r g b

type ArbColorWithIndex(i, j, color) =
    member _.I = i
    member _.J = j
    member _.Color = color

type ArbCanvas(w, h, colorsToSet) =
    let canvas = Canvas(w, h)
    member _.Canvas = canvas
    member _.ColorsToSet = colorsToSet

let genColorTriple =
    let lower = 0
    let upper = 255

    let genFloat =
        Gen.choose (lower, upper) |> Gen.map (fun i -> float32 i / float32 upper)

    Gen.three genFloat

type Generators =
    static member ArbCanvas() =
        let genExpression =
            gen {
                let! w = Gen.choose (1, 300)
                let! h = Gen.choose (1, 300)
                let genI = Gen.choose (0, w - 1)
                let genJ = Gen.choose (0, h - 1)
                let genIndices = Gen.zip genI genJ
                let genColors = genColorTriple |> Gen.map (fun (r, g, b) -> color r g b)

                let! colors =
                    Gen.arrayOf (
                        Gen.zip genIndices genColors
                        |> Gen.map (fun ((i, j), color) -> ArbColorWithIndex(i, j, color))
                    )

                return ArbCanvas(w, h, colors)
            }

        Arb.fromGen (genExpression)

    static member ArbColor() =
        Arb.fromGen (genColorTriple |> Gen.map ArbColor)

    static member Canvas() =
        let genDimension = Gen.choose (1, 300)
        let gen = Gen.two genDimension |> Gen.map Canvas
        Arb.fromGen (gen)

[<Properties(Arbitrary = [| typeof<Generators> |])>]
module Tests =

    [<Property>]
    let ``Writing pixels to a canvas`` (canvasData: ArbCanvas) =
        let canvas = canvasData.Canvas

        canvasData.ColorsToSet
        |> Array.forall (fun colorData ->
            canvas.WritePixel(colorData.I, colorData.J, colorData.Color)
            canvas[colorData.I, colorData.J] = colorData.Color)

    [<Property>]
    let ``Constructing the PPM header`` (canvas: Canvas) =
        let s = canvas.ToPPM()
        let lines = s.Split('\n')[0..2]
        lines = [| "P3"; sprintf "%d %d" canvas.Width canvas.Height; "255" |]

    [<Property>]
    let ``Constructing the PPM pixel data`` (canvas: ArbCanvas) =
        let c = canvas.Canvas

        canvas.ColorsToSet
        |> Array.iter (fun colorData -> c.WritePixel(colorData.I, colorData.J, colorData.Color))

        true
