module FRay.Test.Arbitrary

open FsCheck
open System.Numerics

// Note: These floats are obviously too uniform, but the may suffice
// for generating some properties of matrices.
let genGoodFloat = Gen.choose (0, 65535) |> Gen.map float32

let genGoodVector3 =
    Gen.three genGoodFloat |> Gen.map (fun (x, y, z) -> Vector3(x, y, z))
