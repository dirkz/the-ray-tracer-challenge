namespace FRay.Test.Transformation

open FRay
open FRay.Test.Helpers
open FRay.Test.Arbitrary

open FsCheck.Xunit
open System.Numerics
open FsCheck

type TransformationGenerator =
    static member TranslationMatrix =
        let genMatrix =
            gen {
                let! x = genGoodFloat
                let! y = genGoodFloat
                let! z = genGoodFloat
                let v = Vector3(x, y, z)
                return Matrix4x4.CreateTranslation(v)
            }

        Arb.fromGen (genMatrix)

[<Properties(Arbitrary = [| typeof<TransformationGenerator> |])>]
module TransformationFn =
    [<Property>]
    let ``Check translation`` (translation: Matrix4x4) = true
