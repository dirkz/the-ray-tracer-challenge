module FRay.Test.TupleProperties

open FRay

open FsCheck.Xunit
open System
open System.Numerics

[<Property>]
let ``A tuple with w=1.0 is a point`` x y z =
    let a = tuple x y z 1f
    isPoint a

[<Property>]
let ``A tuple with w=0.0 is a vector`` x y z =
    let a = tuple x y z 0f
    isVector a

[<Property>]
let ``Can access tuple components`` x y z w =
    let a = tuple x y z w

    float32Equals a.X x
    && float32Equals a.Y y
    && float32Equals a.Z z
    && float32Equals a.W w

[<Property>]
let ``point() creates tuples with w=1`` x y z = point x y z |> isPoint

[<Property>]
let ``vector() creates tuples with w=0`` x y z = vector x y z |> isVector

[<Property>]
let ``tuple equality`` x y z w =
    let a = tuple x y z w
    let b = tuple x y z w
    a = b

[<Property>]
let ``point equality`` x y z =
    let a = point x y z
    let b = point x y z
    a = b

[<Property>]
let ``vector equality`` x y z =
    let a = vector x y z
    let b = vector x y z
    a = b

[<Property>]
let ``Adding two tuples`` x1 y1 z1 w1 x2 y2 z2 w2 =
    let a = tuple x1 y1 z1 w1
    let b = tuple x2 y2 z2 w2
    let c = tuple (x1 + x2) (y1 + y2) (z1 + z2) (w1 + w2)
    let d = a + b
    c = d

[<Property>]
let ``Adding two vectors`` x1 y1 z1 x2 y2 z2 =
    let a = vector x1 y1 z1
    let b = vector x2 y2 z2
    let c = vector (x1 + x2) (y1 + y2) (z1 + z2)
    let d = a + b
    c = d

[<Property>]
let ``Adding two vectors is a vector`` x1 y1 z1 x2 y2 z2 =
    let a = vector x1 y1 z1
    let b = vector x2 y2 z2
    let c = a + b
    isVector c

[<Property>]
let ``Adding a vector to a point`` x1 y1 z1 x2 y2 z2 =
    let a = point x1 y1 z1
    let b = vector x2 y2 z2
    let c = point (x1 + x2) (y1 + y2) (z1 + z2)
    let d = a + b
    c = d

[<Property>]
let ``Adding a vector to a point is a point`` x1 y1 z1 x2 y2 z2 =
    let a = point x1 y1 z1
    let b = vector x2 y2 z2
    let c = a + b
    isPoint c

[<Property>]
let ``Subtracting two tuples`` x1 y1 z1 w1 x2 y2 z2 w2 =
    let a = tuple x1 y1 z1 w1
    let b = tuple x2 y2 z2 w2
    let c = tuple (x1 - x2) (y1 - y2) (z1 - z2) (w1 - w2)
    let d = a - b
    c = d

[<Property>]
let ``Subtracting two vectors`` x1 y1 z1 x2 y2 z2 =
    let a = vector x1 y1 z1
    let b = vector x2 y2 z2
    let c = vector (x1 - x2) (y1 - y2) (z1 - z2)
    let d = a - b
    c = d

[<Property>]
let ``Subtracting two vectors is a vector`` x1 y1 z1 x2 y2 z2 =
    let a = vector x1 y1 z1
    let b = vector x2 y2 z2
    let c = a - b
    isVector c

[<Property>]
let ``Subtracting a vector from a point`` x1 y1 z1 x2 y2 z2 =
    let a = point x1 y1 z1
    let b = vector x2 y2 z2
    let c = point (x1 - x2) (y1 - y2) (z1 - z2)
    let d = a - b
    c = d

[<Property>]
let ``Subtracting a vector from a point is a point`` x1 y1 z1 x2 y2 z2 =
    let a = point x1 y1 z1
    let b = vector x2 y2 z2
    let c = a - b
    isPoint c

[<Property>]
let ``Adding points does not yield a point`` x1 y1 z1 x2 y2 z2 =
    let a = point x1 y1 z1
    let b = point x2 y2 z2
    let c = a + b
    isPoint c |> not

[<Property>]
let ``Adding points does not yield a vector`` x1 y1 z1 x2 y2 z2 =
    let a = point x1 y1 z1
    let b = point x2 y2 z2
    let c = a + b
    isVector c |> not

[<Property>]
let ``The difference of two points is a vector`` x1 y1 z1 x2 y2 z2 =
    let a = point x1 y1 z1
    let b = point x2 y2 z2
    let c = a - b
    isVector c

[<Property>]
let ``Negate a vector`` x y z =
    let zero = vector 0f 0f 0f
    let a = vector x y z
    let c = zero - a
    let d = -a
    c = d

[<Property>]
let ``Negating a point doesn't make sense`` x y z =
    let zero = point 0f 0f 0f
    let a = point x y z
    let c = zero - a
    let d = -a
    c <> d

[<Property>]
let ``A negated point is not a point`` x y z =
    let a = point x y z
    let b = -a
    isPoint b |> not

[<Property>]
let ``A negated point is not a vector`` x y z =
    let a = point x y z
    let b = -a
    isVector b |> not

[<Property>]
let ``Negate a tuple`` x y z w =
    let zero = tuple 0f 0f 0f 0f
    let a = tuple x y z w
    let c = zero - a
    let d = -a
    c = d

[<Property>]
let ``Multiplying a tuple`` x y z w s =
    let a = tuple x y z w
    let b = tuple (x * s) (y * s) (z * s) (w * s)
    b = a * s

[<Property>]
let ``Dividing a tuple`` x y z w s =
    let a = tuple x y z w
    let b = tuple (x / s) (y / s) (z / s) (w / s)
    b = a / s

[<Property>]
let ``Vector length squared`` x y z =
    let a = vector x y z
    let b = x * x + y * y + z * z
    float32Equals b (a.LengthSquared())

[<Property>]
let ``Vector magnitude`` x y z =
    let a = vector x y z
    let b = x * x + y * y + z * z |> sqrt
    float32Equals b (a.Length())

[<Property>]
let ``Normalized vector is divided by its magnitude`` x y z =
    let a = vector x y z
    let b = Vector4.Normalize(a)
    (a / a.Length()) = b

[<Property>]
let ``Normalized vector has magnitude of 1, 0 or nanf`` x y z =
    let a = vector x y z
    let b = a |> Vector4.Normalize
    let length = b.Length()
    Single.IsNaN(length) || float32Equals length 0f || float32Equals length 1f

[<Property>]
let ``Dot product`` x1 y1 z1 x2 y2 z2 =
    let a = vector x1 y1 z1
    let b = vector x2 y2 z2
    let c = x1 * x2 + y1 * y2 + z1 * z2
    let d = dot a b
    float32Equals c d

[<Property>]
let ``Can access color components as vector`` r g b =
    let a = color r g b
    float32Equals a.X r && float32Equals a.Y g && float32Equals a.Z b

[<Property>]
let ``Can access color components with functions`` r g b =
    let a = color r g b
    float32Equals a.R r && float32Equals a.G g && float32Equals a.B b

[<Property>]
let ``Adding colors`` r1 g1 b1 r2 g2 b2 =
    let c1 = color r1 g1 b1
    let c2 = color r2 g2 b2
    let d = c1 + c2
    d = color (r1 + r2) (g1 + g2) (b1 + b2)

[<Property>]
let ``Subtracting colors`` r1 g1 b1 r2 g2 b2 =
    let c1 = color r1 g1 b1
    let c2 = color r2 g2 b2
    let d = c1 - c2
    d = color (r1 - r2) (g1 - g2) (b1 - b2)

[<Property>]
let ``Multiplying a color by a scalar`` r g b s =
    let c1 = color r g b
    let c2: Vector4 = c1 * s
    c2.DiscardAlpha = color (r * s) (g * s) (b * s)

[<Property>]
let ``Multiplying colors`` r1 g1 b1 r2 g2 b2 =
    let c1 = color r1 g1 b1
    let c2 = color r2 g2 b2
    let d = c1 * c2
    d = color (r1 * r2) (g1 * g2) (b1 * b2)
