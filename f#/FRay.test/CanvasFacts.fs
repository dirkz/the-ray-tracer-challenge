module FRay.Test.CanvasFacts

open FRay

open Xunit

[<Fact>]
let ``Creating a canvas`` () =
    let c = canvas 10 20
    let expected = color 0f 0f 0f

    for j in 0 .. c.Height - 1 do
        for i in 0 .. c.Width - 1 do
            Assert.Equal(expected, c[i, j])

[<Fact>]
let ``Constructing the PPM pixel data`` () =
    let c = canvas 5 3

    let c1 = color 1.5f 0f 0f
    let c2 = color 0f 0.5f 0f
    let c3 = color -0.5f 0f 1f

    c.WritePixel(0, 0, c1)
    c.WritePixel(2, 1, c2)
    c.WritePixel(4, 2, c3)

    let s = c.ToPPM()

    let lines = s.Split('\n')[0..5]

    Assert.Equal("255 0 0 0 0 0 0 0 0 0 0 0 0 0 0", lines[3])
    Assert.Equal("0 0 0 0 0 0 0 128 0 0 0 0 0 0 0", lines[4])
    Assert.Equal("0 0 0 0 0 0 0 0 0 0 0 0 0 0 255", lines[5])

[<Fact>]
let ``Splitting long lines in PPM files`` () =
    let c = canvas 10 2

    for i in 0 .. c.Width - 1 do
        for j in 0 .. c.Height - 1 do
            c.WritePixel(i, j, color 1f 0.8f 0.6f)

    let s = c.ToPPM()

    let lines = s.Split('\n')[0..6]

    Assert.Equal("255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204", lines[3])
    Assert.Equal("153 255 204 153 255 204 153 255 204 153 255 204 153", lines[4])
    Assert.Equal("255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204", lines[5])
    Assert.Equal("153 255 204 153 255 204 153 255 204 153 255 204 153", lines[6])

[<Fact>]
let ``PPM files are terminated by a newline character`` () =
    let c = canvas 5 3
    let s = c.ToPPM()
    let lines = s.Split('\n')
    Assert.Equal("", lines[lines.Length - 1])
