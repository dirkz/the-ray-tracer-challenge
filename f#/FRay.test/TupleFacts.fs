module FRay.Test.TupleFacts

open FRay

open Xunit
open System.Numerics

[<Fact>]
let ``Vector4.Length of zero`` () =
    let v = Vector4.Zero
    let ls = v.Length()
    Assert.Equal(0f, ls)

[<Fact>]
let ``Vector4.Length of 0,0,0,1`` () =
    let v = Vector4(0f, 0f, 0f, 1f)
    let ls = v.Length()
    Assert.Equal(1f, ls)

[<Fact>]
let ``Vector4.Length of nanf,0,0,0`` () =
    let v = Vector4(nanf, 0f, 0f, 0f)
    Assert.Equal(nanf, v.Length())

[<Fact>]
let ``Vector4.Length of 0,0,nanf,0`` () =
    let v = Vector4(0f, 0f, nanf, 0f)
    Assert.Equal(nanf, v.Length())

[<Fact>]
let ``Normalized vector of 0,0,0,infinityf`` () =
    let a = Vector4(0f, 0f, infinityf, 0f)
    let b = Vector4.Normalize(a)
    Assert.Equal(Vector4(0f, 0f, nanf, 0f), b)
    Assert.Equal(nanf, b.Length())

[<Fact>]
let ``Big, normalized vector has zero length`` () =
    let a = Vector4(0f, 3.402823466e+38f, 0f, 0f)
    let b = Vector4.Normalize(a)
    Assert.Equal(0f, b.Length())

[<Fact>]
let ``The cross product of two vectors`` () =
    let a = vector 1f 2f 3f
    let b = vector 2f 3f 4f
    Assert.Equal(vector -1f 2f -1f, cross a b)
    Assert.Equal(vector 1f -2f 1f, cross b a)
    Assert.Equal(dot (cross a b) a, 0f)
    Assert.Equal(dot (cross b a) a, 0f)
    Assert.Equal(dot (cross a b) b, 0f)
    Assert.Equal(dot (cross b a) b, 0f)
