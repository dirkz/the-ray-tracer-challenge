module FRay.Test.Matrix

open FRay
open FRay.Test.Helpers
open FRay.Test.Arbitrary

open FsCheck.Xunit
open System.Numerics
open FsCheck

[<Property>]
let ``Constructing and inspecting a 4x4 matrix`` m11 m12 m13 m14 m21 m22 m23 m24 m31 m32 m33 m34 m41 m42 m43 m44 =
    let m =
        Matrix4x4(m11, m12, m13, m14, m21, m22, m23, m24, m31, m32, m33, m34, m41, m42, m43, m44)

    float32TripleEquals m11 m[0, 0] m.M11
    && float32TripleEquals m12 m[0, 1] m.M12
    && float32TripleEquals m13 m[0, 2] m.M13
    && float32TripleEquals m14 m[0, 3] m.M14

    && float32TripleEquals m21 m[1, 0] m.M21
    && float32TripleEquals m22 m[1, 1] m.M22
    && float32TripleEquals m23 m[1, 2] m.M23
    && float32TripleEquals m24 m[1, 3] m.M24

    && float32TripleEquals m31 m[2, 0] m.M31
    && float32TripleEquals m32 m[2, 1] m.M32
    && float32TripleEquals m33 m[2, 2] m.M33
    && float32TripleEquals m34 m[2, 3] m.M34

    && float32TripleEquals m41 m[3, 0] m.M41
    && float32TripleEquals m42 m[3, 1] m.M42
    && float32TripleEquals m43 m[3, 2] m.M43
    && float32TripleEquals m44 m[3, 3] m.M44

[<Property>]
let ``A 3x3 matrix ought to be representable`` m11 m12 m13 m21 m22 m23 m31 m32 m33 =
    let m = Matrix3x3(m11, m12, m13, m21, m22, m23, m31, m32, m33)

    float32TripleEquals m11 m[0, 0] m.M11
    && float32TripleEquals m12 m[0, 1] m.M12
    && float32TripleEquals m13 m[0, 2] m.M13

    && float32TripleEquals m21 m[1, 0] m.M21
    && float32TripleEquals m22 m[1, 1] m.M22
    && float32TripleEquals m23 m[1, 2] m.M23

    && float32TripleEquals m31 m[2, 0] m.M31
    && float32TripleEquals m32 m[2, 1] m.M32
    && float32TripleEquals m33 m[2, 2] m.M33

[<Property>]
let ``A 2x2 matrix ought to be representable`` m11 m12 m21 m22 =
    let m = Matrix2x2(m11, m12, m21, m22)

    float32TripleEquals m11 m[0, 0] m.M11
    && float32TripleEquals m12 m[0, 1] m.M12

    && float32TripleEquals m21 m[1, 0] m.M21
    && float32TripleEquals m22 m[1, 1] m.M22

[<Property>]
let ``Matrix4x4 equality with manual parameters`` m11 m12 m13 m14 m21 m22 m23 m24 m31 m32 m33 m34 m41 m42 m43 m44 =
    let m1 =
        Matrix4x4(m11, m12, m13, m14, m21, m22, m23, m24, m31, m32, m33, m34, m41, m42, m43, m44)

    let m2 =
        Matrix4x4(m11, m12, m13, m14, m21, m22, m23, m24, m31, m32, m33, m34, m41, m42, m43, m44)

    m1 = m2

[<Property>]
let ``Matrix3x3 equality`` m11 m12 m13 m21 m22 m23 m31 m32 m33 =
    let m1 = Matrix3x3(m11, m12, m13, m21, m22, m23, m31, m32, m33)

    let m2 = Matrix3x3(m11, m12, m13, m21, m22, m23, m31, m32, m33)

    m1 = m2

[<Property>]
let ``Matrix2x2 equality`` m11 m12 m21 m22 =
    let m1 = Matrix2x2(m11, m12, m21, m22)

    let m2 = Matrix2x2(m11, m12, m21, m22)

    m1 = m2

type MatrixGenerator =
    static member Matrix2x2() =
        let genMatrix2x2 =
            gen {
                let! ms = Gen.arrayOfLength 4 genGoodFloat

                return Matrix2x2(ms[0], ms[1], ms[2], ms[3])
            }

        let arbMatrix2x2 = Arb.fromGen (genMatrix2x2)

        arbMatrix2x2

    static member Matrix3x3() =
        let genMatrix3x3 =
            gen {
                let! ms = Gen.arrayOfLength 9 genGoodFloat

                return Matrix3x3(ms[0], ms[1], ms[2], ms[3], ms[4], ms[5], ms[6], ms[7], ms[8])
            }

        let arbMatrix3x3 = Arb.fromGen (genMatrix3x3)

        arbMatrix3x3

    static member Matrix4x4() =
        let genMatrix4x4 =
            gen {
                let! ms = Gen.arrayOfLength 16 genGoodFloat

                return
                    Matrix4x4(
                        ms[0],
                        ms[1],
                        ms[2],
                        ms[3],
                        ms[4],
                        ms[5],
                        ms[6],
                        ms[7],
                        ms[8],
                        ms[9],
                        ms[10],
                        ms[11],
                        ms[12],
                        ms[13],
                        ms[14],
                        ms[15]
                    )
            }

        let arbMatrix4x4 = Arb.fromGen (genMatrix4x4)

        arbMatrix4x4

[<Properties(Arbitrary = [| typeof<MatrixGenerator> |])>]
module Matrix4x4Properties =

    [<Property>]
    let ``A Matrix2x2 is equal to itself`` (m: Matrix2x2) = m = m

    [<Property>]
    let ``A Matrix3x3 is equal to itself`` (m: Matrix3x3) = m = m

    [<Property>]
    let ``A Matrix4x4 is equal to itself`` (m: Matrix4x4) = m = m

    [<Property>]
    let ``Matrix4x4 multiplication with identity`` (a: Matrix4x4) =
        let id = Matrix4x4.Identity
        let b = a * id
        a.MyEquals(b) && a = b

    [<Property>]
    let ``Matrix2x2 transpose transpose is the original`` (a: Matrix2x2) =
        let t1 = a.Transposed()
        let t2 = t1.Transposed()
        a = t2

    [<Property>]
    let ``Matrix3x3 transpose transpose is the original`` (a: Matrix3x3) =
        let t1 = a.Transposed()
        let t2 = t1.Transposed()
        a = t2

    [<Property>]
    let ``Matrix4x4 transpose transpose is the original`` (a: Matrix4x4) =
        let t1 = a.Transposed()
        let t2 = t1.Transposed()
        a = t2
