module FRay.Test.MatrixFacts

open FRay

open Xunit
open System.Numerics

[<Fact>]
let ``Matrix3x3 equality with different matrices`` () =
    let m1 = Matrix3x3(1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 9f)
    let m2 = Matrix3x3(1f, 2f, 3f, 4f, 5f, 6f, 1f, 2f, 3f)
    Assert.NotEqual(m1, m2)

[<Fact>]
let ``Matrix2x2 equality with different matrices`` () =
    let m1 = Matrix2x2(1f, 2f, 3f, 4f)
    let m2 = Matrix2x2(3f, 4f, 3f, 4f)
    Assert.NotEqual(m1, m2)

[<Fact>]
let ``The transpose of the Matrix2x2 identity is the identity matrix`` () =
    let id = Matrix2x2.Identity
    let transposed = id.Transposed()
    id = transposed

[<Fact>]
let ``The transpose of the Matrix3x3 identity is the identity matrix`` () =
    let id = Matrix3x3.Identity
    let transposed = id.Transposed()
    id = transposed

[<Fact>]
let ``The transpose of the Matrix4x4 identity is the identity matrix`` () =
    let id = Matrix4x4.Identity
    let transposed = id.Transposed()
    id = transposed

[<Fact>]
let ``Calculating the determinant of a 2x2 matrix``() =
    let a = Matrix2x2(1f, 5f, -3f, 2f)
    Assert.Equal(17f, a.Determinant())
