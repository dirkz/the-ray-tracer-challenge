module FRay.Test.Helpers

open FRay

let float32TripleEquals a b c = float32Equals a b && float32Equals a c
