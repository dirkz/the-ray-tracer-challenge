#include <numbers>
#include <stdexcept>

#include <gtest/gtest.h>

#include "matrix.hh"
#include "tuple.hh"

using namespace zrt;

TEST(MatrixTest, MatrixConstructionSimple)
{
    Matrix2x2 empty{};
    for (auto j = 0; j < empty.num_rows(); ++j) {
        for (auto i = 0; i < empty.num_cols(); ++i) {
            ASSERT_EQ(0.0, empty[j][i]);
        }
    }

    Matrix2x2 little{{1}, {2}};
    ASSERT_EQ(1.0, little[0][0]);
    ASSERT_EQ(0.0, little[0][1]);
    ASSERT_EQ(2.0, little[1][0]);
    ASSERT_EQ(0.0, little[1][1]);

    Matrix2x2 exact{{1, 2}, {3, 4}};
    ASSERT_EQ(1.0, exact[0][0]);
    ASSERT_EQ(2.0, exact[0][1]);
    ASSERT_EQ(3.0, exact[1][0]);
    ASSERT_EQ(4.0, exact[1][1]);
}

TEST(MatrixTest, MatrixConstructionErrorTooManyRows)
{
    try {
        Matrix2x2 m1{{0}, {0}, {0}};
        FAIL();
    } catch (const std::invalid_argument &e) {
        ASSERT_EQ(std::string{"2,2 matrix with too many rows (3)"}, e.what());
    }
}

TEST(MatrixTest, MatrixConstructionErrorTooManyColumns)
{
    try {
        Matrix2x2 m1{{0}, {0, 2, 3}};
        FAIL();
    } catch (const std::invalid_argument &e) {
        ASSERT_EQ(std::string{"2,2 matrix row 1 with too many elements (3)"}, e.what());
    }
}

// Constructing and inspecting a 4x4 matrix
TEST(MatrixTest, ConstructingInspecting4x4Matrix)
{
    auto m =
        Matrix4x4{{1, 2, 3, 4}, {5.5, 6.5, 7.5, 8.5}, {9, 10, 11, 12}, {13.5, 14.5, 15.5, 16.5}};
    ASSERT_EQ(1, m[0][0]);
    ASSERT_EQ(4, m[0][3]);
    ASSERT_EQ(5.5, m[1][0]);
    ASSERT_EQ(7.5, m[1][2]);
    ASSERT_EQ(11, m[2][2]);
    ASSERT_EQ(13.5, m[3][0]);
    ASSERT_EQ(15.5, m[3][2]);
}

// A 2x2 matrix ought to be representable
TEST(MatrixTest, Matrix2x2ShouldBeRepresentable)
{
    auto m = Matrix2x2{{-3, 5}, {1, -2}};
    ASSERT_EQ(-3, m[0][0]);
    ASSERT_EQ(5, m[0][1]);
    ASSERT_EQ(1, m[1][0]);
    ASSERT_EQ(-2, m[1][1]);
}

// A 3x3 matrix ought to be representable
TEST(MatrixTest, Matrix3x3ShouldBeRepresentable)
{
    auto m = Matrix3x3{{-3, 5, 0}, {1, -2, -7}, {0, 1, 1}};
    ASSERT_EQ(-3, m[0][0]);
    ASSERT_EQ(-2, m[1][1]);
    ASSERT_EQ(1, m[2][2]);
}

// Matrix equality with identical matrices
TEST(MatrixTest, MatrixEqualityWithIdenticalMatrices)
{
    auto a = Matrix4x4{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 8, 7, 6}, {5, 4, 3, 2}};
    auto b = Matrix4x4{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 8, 7, 6}, {5, 4, 3, 2}};
    ASSERT_EQ(a, b);
}

// Matrix equality with different matrices
TEST(MatrixTest, MatrixEqualityWithDifferentMatrices)
{
    auto a = Matrix4x4{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 8, 7, 6}, {5, 4, 3, 2}};
    auto b = Matrix4x4{{1, 2, 3, 4}, {6, 7, 8, 9}, {8, 7, 6, 5}, {4, 3, 2, 1}};
    ASSERT_NE(a, b);
}

TEST(MatrixTest, SetSingleElements)
{
    auto m = Matrix4x4{};
    m[0][0] = 5;
    m[3][3] = 5;
    ASSERT_EQ(5, m[0][0]);
    ASSERT_EQ(5, m[3][3]);
    ASSERT_EQ(0, m[0][1]);
}

// Multiplying two matrices
TEST(MatrixTest, MultiplyingTwoMatrices)
{
    auto a = Matrix4x4{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 8, 7, 6}, {5, 4, 3, 2}};
    auto b = Matrix4x4{{-2, 1, 2, 3}, {3, 2, 1, -1}, {4, 3, 6, 5}, {1, 2, 7, 8}};
    auto e = Matrix4x4{{20, 22, 50, 48}, {44, 54, 114, 108}, {40, 58, 110, 102}, {16, 26, 46, 42}};
    Matrix4x4 m = a * b;
    ASSERT_EQ(e, m);
}

// A matrix multiplied by a tuple
TEST(MatrixTest, MatrixMultipliedByTuple)
{
    auto a = Matrix4x4{{1, 2, 3, 4}, {2, 4, 4, 2}, {8, 6, 4, 1}, {0, 0, 0, 1}};
    auto b = tuple(1, 2, 3, 1);
    auto e = tuple(18, 24, 33, 1);
    Tuple<double> t = a * b;
    ASSERT_EQ(e, t);
}

// Multiplying a matrix by the identity matrix
TEST(MatrixTest, MultipyingMatrixByIdentity)
{
    auto a = Matrix4x4{{0, 1, 2, 4}, {1, 2, 4, 8}, {2, 4, 8, 16}, {4, 8, 16, 32}};
    ASSERT_EQ(a, a * identity_matrix);
}

// Multiplying the identity matrix by a tuple
TEST(MatrixTest, MultipyingIdentityByTuple)
{
    auto a = tuple(1, 2, 3, 4);
    ASSERT_EQ(a, identity_matrix * a);
}

// Transposing a matrix
TEST(MatrixTest, TransposingMatrix)
{
    auto a = Matrix4x4{{0, 9, 3, 0}, {9, 8, 0, 8}, {1, 8, 5, 3}, {0, 0, 5, 8}};
    auto e = Matrix4x4{{0, 9, 1, 0}, {9, 8, 8, 0}, {3, 0, 5, 5}, {0, 8, 3, 8}};
    ASSERT_EQ(e, a.transposed());
}

// Transposing the identity matrix
TEST(MatrixTest, TransposingIdentityMatrix)
{
    ASSERT_EQ(identity_matrix, identity_matrix.transposed());
}

// Calculating the determinant of a 2x2 matrix
TEST(MatrixTest, CalculatingDeterminantOfMatrix2x2)
{
    auto m = Matrix2x2{{1, 5}, {-3, 2}};
    ASSERT_EQ(17, m.determinant());
}

// A submatrix of a 3x3 matrix is a 2x2 matrix
TEST(MatrixTest, Submatrix3x3IsMatrix2x2)
{
    auto a = Matrix3x3{{1, 5, 0}, {-3, 2, 7}, {0, 6, -3}};
    auto e = Matrix2x2{{-3, 2}, {0, 6}};
    ASSERT_EQ(e, a.submatrix(0, 2));
}

// A submatrix of a 4x4 matrix is a 3x3 matrix
TEST(MatrixTest, Submatrix4x4IsMatrix3x3)
{
    auto a = Matrix4x4{{-6, 1, 1, 6}, {-8, 5, 8, 6}, {-1, 0, 8, 2}, {-7, 1, -1, 1}};
    auto e = Matrix3x3{{-6, 1, 6}, {-8, 8, 6}, {-7, -1, 1}};
    ASSERT_EQ(e, a.submatrix(2, 1));
}

// Calculating a minor of a 3x3 matrix
TEST(MatrixTest, CalculatingMinorMatrix3x3)
{
    auto a = Matrix3x3{{3, 5, 0}, {2, -1, -7}, {6, -1, 5}};
    auto b = a.submatrix(1, 0);
    ASSERT_EQ(25, b.determinant());
    ASSERT_EQ(25, a.minor(1, 0));
}

// Calculating a cofactor of a 3x3 matrix
TEST(MatrixTest, CalculatingCofactorMatrix3x3)
{
    auto a = Matrix3x3{{3, 5, 0}, {2, -1, -7}, {6, -1, 5}};
    ASSERT_EQ(-12, a.minor(0, 0));
    ASSERT_EQ(-12, a.cofactor(0, 0));
    ASSERT_EQ(25, a.minor(1, 0));
    ASSERT_EQ(-25, a.cofactor(1, 0));
}

// Calculating the determinant of a 3x3 matrix
TEST(MatrixTest, CalculatingDeterminantMatrix3x3)
{
    auto a = Matrix3x3{{1, 2, 6}, {-5, 8, -4}, {2, 6, 4}};
    ASSERT_EQ(56, a.cofactor(0, 0));
    ASSERT_EQ(12, a.cofactor(0, 1));
    ASSERT_EQ(-46, a.cofactor(0, 2));
    ASSERT_EQ(-196, a.determinant());
}

// Calculating the determinant of a 4x4 matrix
TEST(MatrixTest, CalculatingDeterminantMatrix4x4)
{
    auto a = Matrix4x4{{-2, -8, 3, 5}, {-3, 1, 7, 3}, {1, 2, -9, 6}, {-6, 7, 7, -9}};
    ASSERT_EQ(690, a.cofactor(0, 0));
    ASSERT_EQ(447, a.cofactor(0, 1));
    ASSERT_EQ(210, a.cofactor(0, 2));
    ASSERT_EQ(51, a.cofactor(0, 3));
    ASSERT_EQ(-4071, a.determinant());
}

// Testing an invertible matrix for invertibility
TEST(MatrixTest, InvertibleMatrix)
{
    auto a = Matrix4x4{{6, 4, 4, 4}, {5, 5, 7, 6}, {4, -9, 3, -7}, {9, 1, 7, -6}};
    ASSERT_EQ(-2120, a.determinant());
    ASSERT_TRUE(a.is_invertible());
}

// Testing a noninvertible matrix for invertibility
TEST(MatrixTest, NonInvertibleMatrix)
{
    auto a = Matrix4x4{{-4, 2, -2, -3}, {9, 6, 2, 6}, {0, -5, 1, -5}, {0, 0, 0, 0}};
    ASSERT_EQ(0, a.determinant());
    ASSERT_FALSE(a.is_invertible());
}

// Calculating the inverse of a matrix
TEST(MatrixTest, InverseMatrix)
{
    auto a = Matrix4x4{{-5, 2, 6, -8}, {1, -5, 1, 8}, {7, 7, -6, -7}, {1, -3, 7, 4}};
    auto b = a.inverted();
    ASSERT_EQ(532, a.determinant());
    ASSERT_EQ(-160, a.cofactor(2, 3));
    ASSERT_EQ(-160.0 / 532.0, b[3][2]);
    ASSERT_EQ(105.0 / 532.0, b[2][3]);
    auto e = Matrix4x4{{0.21805, 0.45113, 0.24060, -0.04511},
                       {-0.80827, -1.45677, -0.44361, 0.52068},
                       {-0.07895, -0.22368, -0.05263, 0.19737},
                       {-0.52256, -0.81391, -0.30075, 0.30639}};
    ASSERT_EQ(e, b);
}

// Calculating the inverse of another matrix
TEST(MatrixTest, InverseMatrixOther)
{
    auto a = Matrix4x4{{8, -5, 9, 2}, {7, 5, 6, 1}, {-6, 0, 9, 6}, {-3, 0, -9, -4}};
    auto b = a.inverted();
    auto e = Matrix4x4{{-0.15385, -0.15385, -0.28205, -0.53846},
                       {-0.07692, 0.12308, 0.02564, 0.03077},
                       {0.35897, 0.35897, 0.43590, 0.92308},
                       {-0.69231, -0.69231, -0.76923, -1.92308}};
    ASSERT_EQ(e, b);
}

// Calculating the inverse of a third matrix
TEST(MatrixTest, InverseMatrixThird)
{
    auto a = Matrix4x4{{9, 3, 0, 9}, {-5, -2, -6, -3}, {-4, 9, 6, 4}, {-7, 6, 6, 2}};
    auto b = a.inverted();
    auto e = Matrix4x4{{-0.04074, -0.07778, 0.14444, -0.22222},
                       {-0.07778, 0.03333, 0.36667, -0.33333},
                       {-0.02901, -0.14630, -0.10926, 0.12963},
                       {0.17778, 0.06667, -0.26667, 0.33333}};
    ASSERT_EQ(e, b);
}

// Multiplying a product by its inverse
TEST(MatrixTest, MultiplyingProductByInverse)
{
    auto a = Matrix4x4{{3, -9, 7, 3}, {3, -8, 2, -9}, {-4, 4, 4, 1}, {-6, 5, -1, 1}};
    auto b = Matrix4x4{{8, 2, 2, 2}, {3, -1, 7, 0}, {7, 0, 5, 4}, {6, -2, 0, 5}};
    auto c = a * b;
    ASSERT_EQ(a, c * b.inverted());
}

// Multiplying by a translation matrix
TEST(MatrixTest, MultiplyingByTranslationMatrix)
{
    auto transform = translation(5, -3, 2);
    auto p = point(-3, 4, 5);
    auto e = point(2, 1, 7);
    auto p2 = transform * p;
    ASSERT_EQ(e, p2);
}

// Multiplying by the inverse of a translation matrix
TEST(MatrixTest, MultiplyingByInverseOfTranslation)
{
    auto transform = translation(5, -3, 2);
    auto inv = transform.inverted();
    auto p = point(-3, 4, 5);
    ASSERT_EQ(point(-8, 7, 3), inv * p);
}

// Translation does not affect vectors
TEST(MatrixTest, TranslationDoesNotAffectVectors)
{
    auto transform = translation(5, -3, 2);
    auto v = vector(-3, 4, 5);
    ASSERT_EQ(v, transform * v);
}

// A scaling matrix applied to a point
TEST(MatrixTest, ScalingMatrixAppliedToPoint)
{
    auto transform = scaling(2, 3, 4);
    auto p = point(-4, 6, 8);
    ASSERT_EQ(point(-8, 18, 32), transform * p);
}

// A scaling matrix applied to a vector
TEST(MatrixTest, ScalingMatrixAppliedToVector)
{
    auto transform = scaling(2, 3, 4);
    auto v = vector(-4, 6, 8);
    ASSERT_EQ(vector(-8, 18, 32), transform * v);
}

// Multiplying by the inverse of a scaling matrix
TEST(MatrixTest, MultiplyingByInverseOfScalingMatrix)
{
    auto transform = scaling(2, 3, 4);
    auto inv = inverse(transform);
    auto v = vector(-4, 6, 8);
    ASSERT_EQ(vector(-2, 2, 2), inv * v);
}

// Reflection is scaling by a negative value
TEST(MatrixTest, ReflectionIsScalingByNegativeValue)
{
    auto transform = scaling(-1, 1, 1);
    auto p = point(2, 3, 4);
    ASSERT_EQ(point(-2, 3, 4), transform * p);
}

const double half_sqrt2 = std::sqrt(2) / 2;
const double pi = std::numbers::pi;

// Rotating a point around the x axis
TEST(MatrixTest, RotatingPointAroundXAxis)
{
    auto p = point(0, 1, 0);
    auto half_quarter = rotation_x(pi / 4);
    auto full_quarter = rotation_x(pi / 2);
    ASSERT_EQ(point(0, half_sqrt2, half_sqrt2), half_quarter * p);
    ASSERT_EQ(point(0, 0, 1), full_quarter * p);
}

// The inverse of an x-rotation rotates in the opposite direction
TEST(MatrixTest, InverseOfXRotationRotatesInOppositeDirection)
{
    auto p = point(0, 1, 0);
    auto half_quarter = rotation_x(pi / 4);
    auto inv = inverse(half_quarter);
    ASSERT_EQ(point(0, half_sqrt2, -half_sqrt2), inv * p);
}

// Rotating a point around the y axis
TEST(MatrixTest, RotatingPointAroundYAxis)
{
    auto p = point(0, 0, 1);
    auto half_quarter = rotation_y(pi / 4);
    auto full_quarter = rotation_y(pi / 2);
    ASSERT_EQ(point(half_sqrt2, 0, half_sqrt2), half_quarter * p);
    ASSERT_EQ(point(1, 0, 0), full_quarter * p);
}

// Rotating a point around the z axis
TEST(MatrixTest, RotatingPointAroundZAxis)
{
    auto p = point(0, 1, 0);
    auto half_quarter = rotation_z(pi / 4);
    auto full_quarter = rotation_z(pi / 2);
    ASSERT_EQ(point(-half_sqrt2, half_sqrt2, 0), half_quarter * p);
    ASSERT_EQ(point(-1, 0, 0), full_quarter * p);
}

// A shearing transformation moves x in proportion to y
TEST(MatrixTest, ShearingTransformationMovesXInProportionToY)
{
    auto transform = shearing(1, 0, 0, 0, 0, 0);
    auto p = point(2, 3, 4);
    ASSERT_EQ(point(5, 3, 4), transform * p);
}

// A shearing transformation moves x in proportion to z
TEST(MatrixTest, ShearingTransformationMovesXInProportionToZ)
{
    auto transform = shearing(0, 1, 0, 0, 0, 0);
    auto p = point(2, 3, 4);
    ASSERT_EQ(point(6, 3, 4), transform * p);
}

// A shearing transformation moves y in proportion to x
TEST(MatrixTest, ShearingTransformationMovesYInProportionToX)
{
    auto transform = shearing(0, 0, 1, 0, 0, 0);
    auto p = point(2, 3, 4);
    ASSERT_EQ(point(2, 5, 4), transform * p);
}

// A shearing transformation moves y in proportion to z
TEST(MatrixTest, ShearingTransformationMovesYInProportionToZ)
{
    auto transform = shearing(0, 0, 0, 1, 0, 0);
    auto p = point(2, 3, 4);
    ASSERT_EQ(point(2, 7, 4), transform * p);
}

// A shearing transformation moves z in proportion to x
TEST(MatrixTest, ShearingTransformationMovesZInProportionToX)
{
    auto transform = shearing(0, 0, 0, 0, 1, 0);
    auto p = point(2, 3, 4);
    ASSERT_EQ(point(2, 3, 6), transform * p);
}

// A shearing transformation moves z in proportion to y
TEST(MatrixTest, ShearingTransformationMovesZInProportionToY)
{
    auto transform = shearing(0, 0, 0, 0, 0, 1);
    auto p = point(2, 3, 4);
    ASSERT_EQ(point(2, 3, 7), transform * p);
}

// Individual transformations are applied in sequence
TEST(MatrixTest, IndividualTransformationsAreAppliedInSequence)
{
    auto p = point(1, 0, 1);
    auto a = rotation_x(pi / 2);
    auto b = scaling(5, 5, 5);
    auto c = translation(10, 5, 7);

    // apply rotation first
    auto p2 = a * p;
    ASSERT_EQ(point(1, -1, 0), p2);

    // then apply scaling
    auto p3 = b * p2;
    ASSERT_EQ(point(5, -5, 0), p3);

    // then apply translation
    auto p4 = c * p3;
    ASSERT_EQ(point(15, 0, 7), p4);
}

// Chained transformations must be applied in reverse order
TEST(MatrixTest, ChainedTransformationsMustBeAppliedInReverseOrder)
{
    auto p = point(1, 0, 1);
    auto a = rotation_x(pi / 2);
    auto b = scaling(5, 5, 5);
    auto c = translation(10, 5, 7);
    auto t = c * b * a;
    ASSERT_EQ(point(15, 0, 7), t * p);
}
