#include <iostream>
#include <iterator>
#include <string>
#include <vector>

#include <gtest/gtest.h>

#include "canvas.hh"

using namespace std;
using namespace zrt;

Canvas<Tuple<double>> canvas(int w, int h)
{
    return canvas<Tuple<double>>(w, h);
}

std::vector<string> split_on(string needle, string haystack)
{
    std::vector<string> result;

    int needle_length = needle.length();
    int from = 0;
    auto current = haystack.find(needle, from);

    while (current != haystack.npos) {
        string s = haystack.substr(from, current - from);
        result.push_back(s);
        from = current + needle_length;
        current = haystack.find(needle, from);
    }

    result.push_back(haystack.substr(from));

    return result;
}

template <class T> std::vector<T> take_n(int c, const std::vector<T> &v)
{
    std::vector<T> result;

    int count = c;
    for (const T &e : v) {
        if (!count) {
            break;
        }
        result.push_back(e);
        --count;
    }

    return result;
}

template <class T> std::vector<T> take_from_to(int from, int to, const std::vector<T> &v)
{
    std::vector<T> result;

    int count = 0;
    for (const T &e : v) {
        if (count > to) {
            break;
        }
        if (count >= from && count <= to) {
            result.push_back(e);
        }
        ++count;
    }

    return result;
}

// Creating a canvas
TEST(CanvasTest, CreatingCanvas)
{
    auto c = canvas(10, 20);
    ASSERT_EQ(10, c.width());
    ASSERT_EQ(20, c.height());
    for (int i = 0; i < c.width(); ++i) {
        for (int j = 0; j < c.height(); ++j) {
            auto t = c.read_pixel(i, j);
            ASSERT_EQ(color(0, 0, 0), t);
        }
    }
}

// Writing pixels to a canvas
TEST(CanvasTest, WritingPixelsToCanvas)
{
    auto c = canvas(10, 20);
    auto red = color(1, 0, 0);
    c.write_pixel(2, 3, red);
    ASSERT_EQ(red, c.read_pixel(2, 3));
}

TEST(CanvasTest, SplitOn1)
{
    string s = "blarg1;blarg2;blarg3";
    std::vector<string> xs = split_on(";", s);
    std::vector<string> expected = {"blarg1", "blarg2", "blarg3"};
    ASSERT_EQ(expected, xs);
}

// Constructing the PPM header
TEST(CanvasTest, ConstructingPPMHeader)
{
    auto c = canvas(5, 3);
    auto ppm = canvas_to_ppm(c);
    auto lines = take_n(3, split_on("\n", ppm));
    std::vector<string> expected = {"P3", "5 3", "255"};
    ASSERT_EQ(expected, lines);
}

// Constructing the PPM pixel data
TEST(CanvasTest, ConstructingPPMPixelData)
{
    auto c = canvas(5, 3);

    auto c1 = color(1.5, 0.0, 0.0);
    auto c2 = color(0.0, 0.5, 0.0);
    auto c3 = color(-0.5, 0.0, 1.0);

    c.write_pixel(0, 0, c1);
    c.write_pixel(2, 1, c2);
    c.write_pixel(4, 2, c3);

    auto ppm = canvas_to_ppm(c);

    std::vector<string> expected = {"255 0 0 0 0 0 0 0 0 0 0 0 0 0 0",
                                    "0 0 0 0 0 0 0 128 0 0 0 0 0 0 0",
                                    "0 0 0 0 0 0 0 0 0 0 0 0 0 0 255"};

    auto all_lines = split_on("\n", ppm);
    auto lines = take_from_to(3, 5, all_lines);
    ASSERT_EQ(expected, lines);
}

// Splitting long lines in PPM files
TEST(CanvasTest, SplittingLongLinesInPPMFiles)
{
    auto c = canvas(10, 2);
    for (int j = 0; j < c.height(); ++j) {
        for (int i = 0; i < c.width(); ++i) {
            c.write_pixel(i, j, color(1.0, 0.8, 0.6));
        }
    }

    auto ppm_lines = split_on("\n", canvas_to_ppm(c));

    auto lines = take_from_to(3, 6, ppm_lines);

    std::vector<string> expected = {
        "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204",
        "153 255 204 153 255 204 153 255 204 153 255 204 153",
        "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204",
        "153 255 204 153 255 204 153 255 204 153 255 204 153"};

    ASSERT_EQ(expected, lines);
}

// PPM files are terminated by a newline character
TEST(CanvasTest, PPMFilesAreTerminatedByNewline)
{
    auto c = canvas(5, 3);
    auto ppm = canvas_to_ppm(c);
    ASSERT_EQ("\n", ppm.substr(ppm.length() - 1));
}
