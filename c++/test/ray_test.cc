#include <gtest/gtest.h>

#include "ray.hh"
#include "tuple.hh"

using namespace zrt;

// Creating and querying a ray
TEST(RayTest, CreatingAndQueryingRay)
{
    auto origin = point(1, 2, 3);
    auto direction = vector(4, 5, 6);
    auto r = ray(origin, direction);
    ASSERT_EQ(origin, r.origin());
    ASSERT_EQ(direction, r.direction());
}

// Computing a point from a distance
TEST(RayTest, ComputingPointFromDistance)
{
    auto r = ray(point(2, 3, 4), vector(1, 0, 0));
    ASSERT_EQ(point(2, 3, 4), position(r, 0));
    ASSERT_EQ(point(3, 3, 4), position(r, 1));
    ASSERT_EQ(point(1, 3, 4), position(r, -1));
    ASSERT_EQ(point(4.5, 3, 4), position(r, 2.5));
}
