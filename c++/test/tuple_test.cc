#include <gtest/gtest.h>

#include "tuple.hh"

using namespace zrt;

// A tuple with w=1.0 is a point
TEST(TupleTest, TupleAsPoint)
{
    auto a = tuple(4.3, -4.2, 3.1, 1.0);

    ASSERT_DOUBLE_EQ(4.3, a.x());
    ASSERT_DOUBLE_EQ(-4.2, a.y());
    ASSERT_DOUBLE_EQ(3.1, a.z());
    ASSERT_DOUBLE_EQ(1, a.w());
    ASSERT_TRUE(a.is_point());
    ASSERT_FALSE(a.is_vector());
}

// A tuple with w=0 is a vector
TEST(TupleTest, TupleAsVector)
{
    auto a = tuple(4.3, -4.2, 3.1, 0.0);

    ASSERT_DOUBLE_EQ(4.3, a.x());
    ASSERT_DOUBLE_EQ(-4.2, a.y());
    ASSERT_DOUBLE_EQ(3.1, a.z());
    ASSERT_DOUBLE_EQ(0, a.w());
    ASSERT_FALSE(a.is_point());
    ASSERT_TRUE(a.is_vector());
}

// point() creates tuples with w=1
TEST(TupleTest, PointCreatesPoints)
{
    ASSERT_EQ(tuple(4, -4, 3, 1), point(4, -4, 3));
}

// vector() creates tuples with w=0
TEST(TupleTest, VectorCreatesVector)
{
    ASSERT_EQ(tuple(4, -4, 3, 0), vector(4, -4, 3));
}

// Adding two tuples
TEST(TupleTest, AddingTwoTuple)
{
    auto a1 = tuple(3, -2, 5, 1);
    auto a2 = tuple(-2, 3, 1, 0);
    ASSERT_EQ(tuple(1, 1, 6, 1), a1 + a2);
}

// Subtracting two points
TEST(TupleTest, SubtractingTwoPoints)
{
    auto p1 = point(3, 2, 1);
    auto p2 = point(5, 6, 7);
    ASSERT_EQ(vector(-2, -4, -6), p1 - p2);
}

// Subtracting a vector from a point
TEST(TupleTest, SubtractingVectorFromPoint)
{
    auto p = point(3, 2, 1);
    auto v = vector(5, 6, 7);
    ASSERT_EQ(point(-2, -4, -6), p - v);
}

// Subtracting two vectors
TEST(TupleTest, SubtractingTwoVectors)
{
    auto v1 = vector(3, 2, 1);
    auto v2 = vector(5, 6, 7);
    ASSERT_EQ(vector(-2, -4, -6), v1 - v2);
}

// Subtracting a vector from the zero vector
TEST(TupleTest, SubtractingVectorFromZero)
{
    auto zero = vector(0, 0, 0);
    auto v = vector(1, -2, 3);
    ASSERT_EQ(vector(-1, 2, -3), zero - v);
}

// Negating a tuple
TEST(TupleTest, NegatingTuple)
{
    auto a = tuple(1, -2, 3, -4);
    ASSERT_EQ(tuple(-1, 2, -3, 4), -a);
}

// Multiplying a tuple by a scalar
TEST(TupleTest, MultiplyingTupleByScalar)
{
    auto a = tuple(1.0, -2.0, 3.0, -4.0);
    ASSERT_EQ(tuple(3.5, -7.0, 10.5, -14.0), a * 3.5);
}

// Multiplying a tuple by a fraction
TEST(TupleTest, MultiplyingTupleByFraction)
{
    auto a = tuple(1.0, -2.0, 3.0, -4.0);
    ASSERT_EQ(tuple(0.5, -1.0, 1.5, -2.0), a * 0.5);
}

// Dividing a tuple by a scalar
TEST(TupleTest, DividingTupleByScalar)
{
    auto a = tuple(1.0, -2.0, 3.0, -4.0);
    ASSERT_EQ(tuple(0.5, -1.0, 1.5, -2.0), a / 2.0);
}

// Computing the magnitude of vector(1, 0, 0)
TEST(TupleTest, ComputingTheMagnitudeOfX1)
{
    auto v = vector(1, 0, 0);
    ASSERT_EQ(1, v.magnitude());
}

// Computing the magnitude of vector(0, 1, 0)
TEST(TupleTest, ComputingTheMagnitudeOfY1)
{
    auto v = vector(0, 1, 0);
    ASSERT_EQ(1, v.magnitude());
}

// Computing the magnitude of vector(0, 0, 1)
TEST(TupleTest, ComputingTheMagnitudeOfZ1)
{
    auto v = vector(0, 0, 1);
    ASSERT_EQ(1, v.magnitude());
}

// Computing the magnitude of vector(1, 2, 3)
TEST(TupleTest, ComputingTheMagnitudeOf123)
{
    auto v = vector(1.0, 2.0, 3.0);
    ASSERT_EQ(sqrt(14.0), v.magnitude());
}

// Computing the magnitude of vector(-1, -2, -3)
TEST(TupleTest, ComputingTheMagnitudeOfMinus123)
{
    auto v = -vector(1.0, 2.0, 3.0);
    ASSERT_EQ(sqrt(14.0), v.magnitude());
}

// Normalizing vector(4, 0, 0) gives (1, 0, 0)
TEST(TupleTest, NormalizingVector400)
{
    auto v = vector(4.0, 0.0, 0.0);
    ASSERT_EQ(vector(1.0, 0.0, 0.0), v.normalized());
}

// Normalizing vector(1, 2, 3)
TEST(TupleTest, NormalizingVector123)
{
    auto v = vector(1.0, 2.0, 3.0);
    auto sqrt14 = sqrt(14.0);
    auto e = vector(1.0 / sqrt14, 2.0 / sqrt14, 3.0 / sqrt14);
    ASSERT_EQ(e, v.normalized());
}

// The magnitude of a normalized vector
TEST(TupleTest, MagnitudeOfNormalizedVector)
{
    auto v = vector(1.0, 2.0, 3.0);
    ASSERT_EQ(1.0, v.normalized().magnitude());
}

// The dot product of two tuples
TEST(TupleTest, DotProductOfTwoTuples)
{
    auto a = vector(1, 2, 3);
    auto b = vector(2, 3, 4);
    ASSERT_EQ(20, dot(a, b));
}

// The cross product of two vectors
TEST(TupleTest, CrossProductOfTwoVectors)
{
    auto a = vector(1, 2, 3);
    auto b = vector(2, 3, 4);
    ASSERT_EQ(vector(-1, 2, -1), cross(a, b));
    ASSERT_EQ(vector(1, -2, 1), cross(b, a));
}

// The cross product of X- and Y-axes, left- and right-handed
TEST(TupleTest, CrossProductXY)
{
    auto x = vector(1, 0, 0);
    auto y = vector(0, 1, 0);
    ASSERT_EQ(vector(0, 0, 1), cross(x, y));
    ASSERT_EQ(vector(0, 0, -1), cross(y, x));
}

// Colors are (red, green, blue) tuples
TEST(TupleTest, ColorsAreRgbTuples)
{
    auto c = color(-0.5, 0.4, 1.7);
    ASSERT_EQ(-0.5, c.r());
    ASSERT_EQ(-0.5, c.red());
    ASSERT_EQ(0.4, c.g());
    ASSERT_EQ(0.4, c.green());
    ASSERT_EQ(1.7, c.b());
    ASSERT_EQ(1.7, c.blue());
}

// Adding colors
TEST(TupleTest, AddingColors)
{
    auto c1 = color(0.9, 0.6, 0.75);
    auto c2 = color(0.7, 0.1, 0.25);
    ASSERT_EQ(color(1.6, 0.7, 1.0), c1 + c2);
}

// Subtracting colors
TEST(TupleTest, SubtractingColors)
{
    auto c1 = color(0.9, 0.6, 0.75);
    auto c2 = color(0.7, 0.1, 0.25);
    ASSERT_EQ(color(0.2, 0.5, 0.5), c1 - c2);
}

// Multiplying a color by a scalar
TEST(TupleTest, MultiplyingColorByScalar)
{
    auto c = color(0.2, 0.3, 0.4);
    ASSERT_EQ(color(0.4, 0.6, 0.8), c * 2.0);
}

// Multiplying colors
TEST(TupleTest, MultiplyingColors)
{
    auto c1 = color(1.0, 0.2, 0.4);
    auto c2 = color(0.9, 1.0, 0.1);
    ASSERT_EQ(color(0.9, 0.2, 0.04), c1 * c2);
}
