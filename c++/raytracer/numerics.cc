#include "numerics.hh"

#include <cmath>

namespace zrt
{

template <> double decimal_round<double>(const double &a, int decimals)
{
    return std::floor(a * double(decimals) + 0.5) / decimals;
}

template <> float decimal_round<float>(const float &a, int decimals)
{
    return std::floor(a * float(decimals) + 0.5) / decimals;
}

} // namespace zrt
