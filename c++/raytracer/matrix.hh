#pragma once

#include <array>
#include <cmath>
#include <initializer_list>
#include <iostream>
#include <stdexcept>

#include <stdlib.h>

#include "numerics.hh"
#include "tuple.hh"

namespace zrt
{

template <size_t M, size_t N, class T> struct Matrix
{
    Matrix()
    {
        for (auto j = 0; j < M; ++j) {
            for (auto i = 0; i < N; ++i) {
                _e[j][i] = T{0};
            }
        }
    }

    Matrix(std::initializer_list<std::initializer_list<T>> rows)
    {
        static size_t error_buffer_len = 128;

        if (rows.size() > M) {
            char buffer[error_buffer_len];
            snprintf(buffer, error_buffer_len, "%lu,%lu matrix with too many rows (%lu)", M, N,
                     rows.size());
            throw std::invalid_argument(buffer);
        }

        size_t row_count = 0;
        for (std::initializer_list<T> row : rows) {
            if (row.size() > N) {
                char buffer[error_buffer_len];
                snprintf(buffer, error_buffer_len,
                         "%lu,%lu matrix row %lu with too many elements (%lu)", M, N, row_count,
                         row.size());
                throw std::invalid_argument(buffer);
            }

            size_t col_count = 0;
            for (const T &e : row) {
                _e[row_count][col_count] = e;
                ++col_count;
            }

            for (auto i = col_count; i < N; ++i) {
                _e[row_count][i] = T{0};
            }

            ++row_count;
        }

        for (auto j = row_count; j < M; ++j) {
            for (auto i = 0; i < N; ++i) {
                _e[j][i] = T{0};
            }
        }
    }

    const std::array<T, N> &operator[](size_t i) const
    {
        return _e[i];
    }

    std::array<T, N> &operator[](size_t i)
    {
        return _e[i];
    }

    size_t num_rows() const
    {
        return M;
    }

    size_t num_cols() const
    {
        return N;
    }

    Matrix<N, M, T> transposed() const
    {
        Matrix<N, M, T> result;

        for (auto j = 0; j < N; ++j) {
            for (auto i = 0; i < M; ++i) {
                result[j][i] = _e[i][j];
            }
        }

        return result;
    }

    template <size_t U = M, size_t V = N>
    typename std::enable_if<U == 2 && V == 2, T>::type determinant() const
    {
        return _e[0][0] * _e[1][1] - _e[0][1] * _e[1][0];
    }

    template <size_t U = M, size_t V = N>
    typename std::enable_if<(U > 2 && V > 2), T>::type determinant() const
    {
        T result{0};

        for (auto i = 0; i < N; ++i) {
            result += _e[0][i] * cofactor(0, i);
        }

        return result;
    }

    Matrix<M - 1, N - 1, T> submatrix(size_t row, size_t col) const
    {
        Matrix<M - 1, N - 1, T> result;

        for (auto j = 0; j < M - 1; ++j) {
            for (auto i = 0; i < N - 1; ++i) {
                auto source_j = j >= row ? j + 1 : j;
                auto source_i = i >= col ? i + 1 : i;
                result[j][i] = _e[source_j][source_i];
            }
        }

        return result;
    }

    T minor(size_t row, size_t col) const
    {
        return submatrix(row, col).determinant();
    }

    T cofactor(size_t row, size_t col) const
    {
        auto the_minor = minor(row, col);
        auto modulo = (row + col) % 2;
        bool is_even = modulo == 0;
        if (is_even) {
            return the_minor;
        } else {
            return -the_minor;
        }
    }

    bool is_invertible() const
    {
        return determinant() != T{0};
    }

    template <size_t U = M, size_t V = N>
    typename std::enable_if<U == V, Matrix<U, V, T>>::type inverted() const
    {
        Matrix<M, N, T> result;
        T d = determinant();

        for (auto j = 0; j < M; ++j) {
            for (auto i = 0; i < N; ++i) {
                T c = cofactor(j, i);
                result[i][j] = c / d;
            }
        }

        return result;
    }

    template <size_t U = M, size_t V = N>
    static typename std::enable_if<U == 4 && V == 4, Matrix<U, V, T>>::type translation(const T x,
                                                                                        const T y,
                                                                                        const T z)
    {
        Matrix<U, V, T> m;

        for (auto j = 0; j < U; ++j) {
            for (auto i = 0; i < V; ++i) {
                if (i == j) {
                    m[j][i] = T{1};
                } else if (i == V - 1) {
                    switch (j) {
                    case 0:
                        m[j][i] = x;
                        break;
                    case 1:
                        m[j][i] = y;
                        break;
                    case 2:
                        m[j][i] = z;
                        break;
                    }
                }
            }
        }

        return m;
    }

    template <size_t U = M, size_t V = N>
    static typename std::enable_if<U == 4 && V == 4, Matrix<U, V, T>>::type scaling(const T x,
                                                                                    const T y,
                                                                                    const T z)
    {
        Matrix<U, V, T> m;

        for (auto j = 0; j < U; ++j) {
            for (auto i = 0; i < V; ++i) {
                if (i == j) {
                    switch (i) {
                    case 0:
                        m[j][i] = x;
                        break;
                    case 1:
                        m[j][i] = y;
                        break;
                    case 2:
                        m[j][i] = z;
                        break;
                    case 3:
                        m[j][i] = T{1};
                        break;
                    }
                }
            }
        }

        return m;
    }

    template <size_t U = M, size_t V = N>
    static typename std::enable_if<U == 4 && V == 4, Matrix<U, V, T>>::type rotation_x(
        const T radians)
    {
        auto s = std::sin(radians);
        auto c = std::cos(radians);

        Matrix<U, V, T> m{{1, 0, 0, 0}, {0, c, -s, 0}, {0, s, c, 0}, {0, 0, 0, 1}};

        return m;
    }

    template <size_t U = M, size_t V = N>
    static typename std::enable_if<U == 4 && V == 4, Matrix<U, V, T>>::type rotation_y(
        const T radians)
    {
        auto s = std::sin(radians);
        auto c = std::cos(radians);

        Matrix<U, V, T> m{{c, 0, s, 0}, {0, 1, 0, 0}, {-s, 0, c, 0}, {0, 0, 0, 1}};

        return m;
    }

    template <size_t U = M, size_t V = N>
    static typename std::enable_if<U == 4 && V == 4, Matrix<U, V, T>>::type rotation_z(
        const T radians)
    {
        auto s = std::sin(radians);
        auto c = std::cos(radians);

        Matrix<U, V, T> m{{c, -s, 0, 0}, {s, c, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};

        return m;
    }

    template <size_t U = M, size_t V = N>
    static typename std::enable_if<U == 4 && V == 4, Matrix<U, V, T>>::type shearing(
        const T xy, const T xz, const T yx, const T yz, const T zx, const T zy)
    {
        Matrix<U, V, T> m{{1, xy, xz, 0}, {yx, 1, yz, 0}, {zx, zy, 1, 0}, {0, 0, 0, 1}};
        return m;
    }

  private:
    std::array<std::array<T, N>, M> _e;
};

template <size_t M, size_t N, class T>
bool operator==(const Matrix<M, N, T> &a, const Matrix<M, N, T> &b)
{
    for (auto j = 0; j < M; ++j) {
        for (auto i = 0; i < N; ++i) {
            if (decimal_round(a[j][i]) != decimal_round(b[j][i])) {
                T n1 = a[j][i];
                T n2 = b[j][i];
                std::cout << "Matrix not equal at (" << j << ", " << i << "): " << n1
                          << " != " << n2 << "\n";
                return false;
            }
        }
    }
    return true;
}

template <size_t M, size_t N, class T>
bool operator!=(const Matrix<M, N, T> &a, const Matrix<M, N, T> &b)
{
    return !(a == b);
}

template <size_t M, size_t N, size_t P, class T>
Matrix<M, P, T> operator*(const Matrix<M, N, T> &a, const Matrix<N, P, T> &b)
{
    Matrix<M, P, T> m{};

    for (auto j = 0; j < M; ++j) {
        for (auto i = 0; i < P; ++i) {
            for (auto column = 0; column < N; ++column) {
                m[j][i] += a[j][column] * b[column][i];
            }
        }
    }

    return m;
}

template <size_t M, size_t N, class T>
std::ostream &operator<<(std::ostream &out, const Matrix<M, N, T> &m)
{
    out << "{ ";
    for (auto j = 0; j < M; ++j) {
        out << "{ ";
        for (auto i = 0; i < N; ++i) {
            out << m[j][i];
            if (i != N - 1) {
                out << ", ";
            }
        }
        if (j == M - 1) {
            out << "}";
        } else {
            out << " }, ";
        }
    }
    return out << " }";
}

// A tuple is a 4x1 matrix:
// Matrix m,4 * Matrix 4,1 -> Matrix m,1
// Matrix 4,4 * Matrix 4,1 -> Matrix 4,1
template <class T> Tuple<T> operator*(const Matrix<4, 4, T> &m, const Tuple<T> &t)
{
    Tuple<T> result;

    for (auto j = 0; j < 4; ++j) {
        for (auto i = 0; i < 4; ++i) {
            result[j] += m[j][i] * t[i];
        }
    }

    return result;
}

typedef Matrix<4, 4, double> Matrix4x4;
typedef Matrix<3, 3, double> Matrix3x3;
typedef Matrix<2, 2, double> Matrix2x2;

const Matrix4x4 identity_matrix{{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};

Matrix4x4 translation(const double x, const double y, const double z)
{
    return Matrix4x4::translation(x, y, z);
}

Matrix4x4 scaling(const double x, const double y, const double z)
{
    return Matrix4x4::scaling(x, y, z);
}

template <size_t M, size_t N, class T> Matrix<M, N, T> inverse(const Matrix<M, N, T> &m)
{
    return m.inverted();
}

Matrix4x4 rotation_x(const double radians)
{
    return Matrix4x4::rotation_x(radians);
}

Matrix4x4 rotation_y(const double radians)
{
    return Matrix4x4::rotation_y(radians);
}

Matrix4x4 rotation_z(const double radians)
{
    return Matrix4x4::rotation_z(radians);
}

Matrix4x4 shearing(const double xy, const double xz, const double yx, const double yz,
                   const double zx, const double zy)
{
    return Matrix4x4::shearing(xy, xz, yx, yz, zx, zy);
}

} // namespace zrt
