#include "tuple.hh"

namespace zrt
{

Tuple<double> tuple(const double &x, const double &y, const double &z, const double &w)
{
    return Tuple<double>{x, y, z, w};
}

Tuple<double> point(const double &x, const double &y, const double &z)
{
    return tuple(x, y, z, 1);
}

Tuple<double> color(const double &x, const double &y, const double &z)
{
    return tuple(x, y, z, 0);
}

Tuple<double> vector(const double &x, const double &y, const double &z)
{
    return tuple(x, y, z, 0);
}

} // namespace zrt
