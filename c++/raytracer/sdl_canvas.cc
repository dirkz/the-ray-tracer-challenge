#include <SDL2/SDL.h>

#include "sdl_canvas.hh"
#include "sdl_exception.hh"

namespace zrt
{

static unsigned int sdl_subsystems = SDL_INIT_VIDEO;

SDLCanvas::SDLCanvas(int w, int h) : Canvas(w, h)
{
    zdl::check(SDL_InitSubSystem(sdl_subsystems));
    _window = SDL_CreateWindow("The Ray Tracer Challenge", 0, 0, w, h, 0);
    zdl::check(_window);
}

SDLCanvas::~SDLCanvas()
{
    SDL_DestroyWindow(_window);
    SDL_QuitSubSystem(sdl_subsystems);
}

} // namespace zrt
