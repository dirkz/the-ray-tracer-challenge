#pragma once

#include <iostream>
#include <stdexcept>

#include "numerics.hh"

namespace zrt
{

template <class T> struct Tuple
{
    Tuple(const T x = T{0}, const T y = T{0}, const T z = T{0}, const T w = T{0})
        : _x{x}, _y{y}, _z{z}, _w{w}
    {
    }

    Tuple(const Tuple<T> &b) : _x{b.x()}, _y{b.y()}, _z{b.z()}, _w{b.w()}
    {
    }

    bool is_point()
    {
        return _w == 1;
    }

    bool is_vector()
    {
        return _w == 0;
    }

    inline const T &x() const
    {
        return _x;
    }

    inline const T &y() const
    {
        return _y;
    }

    inline const T &z() const
    {
        return _z;
    }

    inline const T &w() const
    {
        return _w;
    }

    inline const T &r() const
    {
        return _x;
    }

    inline const T &g() const
    {
        return _y;
    }

    inline const T &b() const
    {
        return _z;
    }

    inline const T &a() const
    {
        return _w;
    }

    inline const T &red() const
    {
        return _x;
    }

    inline const T &green() const
    {
        return _y;
    }

    inline const T &blue() const
    {
        return _z;
    }

    inline const T &alpha() const
    {
        return _w;
    }

    const T &operator[](size_t j) const
    {
        switch (j) {
        case 0:
            return _x;
        case 1:
            return _y;
        case 2:
            return _z;
        case 3:
            return _w;
        default: {
            const size_t buffer_len = 256;
            char buffer[buffer_len];
            snprintf(buffer, buffer_len, "invalid tuple index: %lu", j);
            throw std::invalid_argument(buffer);
        }
        }
    }

    T &operator[](size_t j)
    {
        switch (j) {
        case 0:
            return _x;
        case 1:
            return _y;
        case 2:
            return _z;
        case 3:
            return _w;
        default: {
            const size_t buffer_len = 256;
            char buffer[buffer_len];
            snprintf(buffer, buffer_len, "invalid tuple index: %lu", j);
            throw std::invalid_argument(buffer);
        }
        }
    }

    Tuple<T> &operator+=(const Tuple<T> &b)
    {
        _x += b.x();
        _y += b.y();
        _z += b.z();
        _w += b.w();

        return *this;
    }

    Tuple<T> &operator-=(const Tuple<T> &b)
    {
        _x -= b.x();
        _y -= b.y();
        _z -= b.z();
        _w -= b.w();

        return *this;
    }

    Tuple<T> &operator-()
    {
        _x = -_x;
        _y = -_y;
        _z = -_z;
        _w = -_w;

        return *this;
    }

    Tuple<T> &operator*=(const Tuple<T> &b)
    {
        _x *= b.x();
        _y *= b.y();
        _z *= b.z();
        _w *= b.w();

        return *this;
    }

    Tuple<T> &operator*=(const T s)
    {
        _x *= s;
        _y *= s;
        _z *= s;
        _w *= s;

        return *this;
    }

    Tuple<T> &operator/=(const T s)
    {
        _x /= s;
        _y /= s;
        _z /= s;
        _w /= s;

        return *this;
    }

    T magnitude()
    {
        return sqrt(_x * _x + _y * _y + _z * _z + _w * _w);
    }

    Tuple<T> normalized()
    {
        auto m = magnitude();
        return Tuple{_x / m, _y / m, _z / m, _w / m};
    }

    T dot(const Tuple<T> &b) const
    {
        return _x * b._x + _y * b._y + _z * b._z + _w * b._w;
    }

    Tuple<T> crossed(const Tuple<T> &b) const
    {
        return Tuple{_y * b._z - _z * b._y, _z * b._x - _x * b._z, _x * b._y - _y * b._x, T{0}};
    }

  private:
    T _x;
    T _y;
    T _z;
    T _w;
};

Tuple<double> tuple(const double &x, const double &y, const double &z, const double &w);
Tuple<double> point(const double &x, const double &y, const double &z);
Tuple<double> color(const double &x, const double &y, const double &z);
Tuple<double> vector(const double &x, const double &y, const double &z);

template <class T> bool operator==(const Tuple<T> &a, const Tuple<T> &b)
{
    return decimal_round(a.x()) == decimal_round(b.x()) &&
           decimal_round(a.y()) == decimal_round(b.y()) &&
           decimal_round(a.z()) == decimal_round(b.z()) &&
           decimal_round(a.w()) == decimal_round(b.w());
}

template <class T> std::ostream &operator<<(std::ostream &out, const Tuple<T> &t)
{
    return out << " { " << t.x() << " " << t.y() << " " << t.z() << " " << t.w() << " }";
}

template <class T> Tuple<T> operator+(const Tuple<T> a, const Tuple<T> b)
{
    Tuple<T> c{a};
    c += b;
    return c;
}

template <class T> Tuple<T> operator-(const Tuple<T> a, const Tuple<T> b)
{
    Tuple<T> c{a};
    c -= b;
    return c;
}

template <class T> Tuple<T> operator*(const Tuple<T> a, const Tuple<T> b)
{
    Tuple<T> c{a};
    c *= b;
    return c;
}

template <class T> Tuple<T> operator*(const Tuple<T> a, const T &s)
{
    Tuple<T> b{a};
    b *= s;
    return b;
}

template <class T> Tuple<T> operator*(const T &s, const Tuple<T> a)
{
    return a * s;
}

template <class T> Tuple<T> operator/(const Tuple<T> a, const T s)
{
    Tuple<T> b{a};
    b /= s;
    return b;
}

template <class T> T dot(const Tuple<T> a, const Tuple<T> b)
{
    return a.dot(b);
}

template <class T> Tuple<T> cross(const Tuple<T> a, const Tuple<T> b)
{
    return a.crossed(b);
}

} // namespace zrt
