#pragma once

namespace zrt
{

template <class T> T decimal_round(const T &a, int decimals = 1000)
{
    return a;
}

template <> double decimal_round<double>(const double &a, int decimals);

template <> float decimal_round<float>(const float &a, int decimals);

} // namespace zrt
