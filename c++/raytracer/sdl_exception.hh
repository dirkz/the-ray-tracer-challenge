#pragma once

#include <exception>
#include <functional>
#include <string>

#include <SDL2/SDL.h>

namespace zdl
{

struct Exception : public std::exception
{
    Exception() : _msg{SDL_GetError()}
    {
    }

    virtual const char *what()
    {
        return _msg.c_str();
    }

  private:
    std::string _msg;
};

void check(std::function<int()> f)
{
    int result = f();
    if (result) {
        throw Exception();
    }
}

void check(int result)
{
    if (!result) {
        throw Exception();
    }
}

template <class T> void check(const T *pointer)
{
    if (!pointer) {
        throw Exception();
    }
}

} // namespace zdl
