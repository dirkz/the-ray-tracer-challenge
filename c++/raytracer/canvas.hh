#pragma once

#include <iostream>
#include <sstream>
#include <vector>

#include "tuple.hh"

namespace zrt
{

template <class T> struct Canvas
{
    Canvas(int w, int h) : _width{w}, _height{h}, _pixels(w * h)
    {
    }

    int width() const
    {
        return _width;
    }

    int height() const
    {
        return _height;
    }

    void write_pixel(int i, int j, const T &c)
    {
        _pixels[j * _width + i] = c;
    }

    T read_pixel(int i, int j) const
    {
        return _pixels[j * _width + i];
    }

    T pixel_at(int i, int j) const
    {
        return read_pixel(i, j);
    }

    virtual ~Canvas() {};

  private:
    int _width;
    int _height;
    std::vector<T> _pixels;
};

template <class T> Canvas<T> canvas(int w, int h)
{
    return Canvas<T>{w, h};
}

template <class T> T read_pixel(const Canvas<T> &canvas, int i, int j)
{
    return canvas.read_pixel(i, j);
}

template <class T> void write_pixel(Canvas<T> &canvas, int i, int j, const T &c)
{
    canvas.write_pixel(i, j, c);
}

int clamp255(int i)
{
    if (i < 0) {
        return 0;
    } else if (i > 255) {
        return 255;
    }
    return i;
}

template <class T> int pixel_color_value(const T &p)
{
    return clamp255(static_cast<int>(round(static_cast<double>(p) * 255.0)));
}

int write_int(std::ostream &out, int pos, int i)
{
    static int max_col = 70;

    std::string s = std::to_string(i);
    int len = s.length();

    if (pos == 0) {
        out << "\n" << s;
        return pos + len;
    } else {
        if (pos + len + 1 < max_col) {
            out << " " << s;
            return pos + len + 1;
        } else {
            out << "\n" << s;
            return len;
        }
    }
}

template <class T> std::ostream &operator<<(std::ostream &out, const Canvas<Tuple<T>> &canvas)
{
    out << "P3\n";
    out << canvas.width() << " " << canvas.height() << "\n";
    out << "255";

    for (int j = 0; j < canvas.height(); ++j) {
        int current_pos = 0;
        for (int i = 0; i < canvas.width(); ++i) {
            auto t = canvas.read_pixel(i, j);
            int r = pixel_color_value(t.r());
            int g = pixel_color_value(t.g());
            int b = pixel_color_value(t.b());
            current_pos = write_int(out, current_pos, r);
            current_pos = write_int(out, current_pos, g);
            current_pos = write_int(out, current_pos, b);
        }
    }

    out << "\n";

    return out;
}

template <class T> std::string canvas_to_ppm(const Canvas<Tuple<T>> &canvas)
{
    std::basic_stringstream<char> out;
    out << canvas;
    std::string s{out.str()};
    return s;
}

}; // namespace zrt
