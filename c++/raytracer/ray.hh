#pragma once

#include "tuple.hh"

namespace zrt
{

template <class T> struct RayT
{
    RayT(const Tuple<T> &origin, const Tuple<T> direction) : _origin{origin}, _direction{direction}
    {
    }

    const Tuple<T> &origin() const
    {
        return _origin;
    }

    const Tuple<T> &direction() const
    {
        return _direction;
    }

    Tuple<T> position(const T t) const
    {
        return _origin + t * _direction;
    }

  private:
    Tuple<T> _origin;
    Tuple<T> _direction;
};

using Ray = RayT<double>;

Ray ray(const Tuple<double> origin, const Tuple<double> direction)
{
    return Ray(origin, direction);
}

Tuple<double> position(const Ray &ray, const double t)
{
    return ray.position(t);
}

} // namespace zrt
