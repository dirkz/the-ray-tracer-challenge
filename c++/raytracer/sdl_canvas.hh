#pragma once

#include <SDL2/SDL.h>

#include "canvas.hh"
#include "tuple.hh"

namespace zrt
{

struct SDLCanvas : public Canvas<Tuple<double>>
{
    SDLCanvas(int w, int h);
    ~SDLCanvas();

  private:
    SDL_Window *_window;
};

} // namespace zrt
