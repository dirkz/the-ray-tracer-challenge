#!/bin/env bash

for dir in raytracer test
do
    find $dir \( -name \*.cc -o -name \*.hh -o -name \*.cpp -o -name \*.hpp -o -name \*.metal \) -exec clang-format-mp-18 -i {} \;
done
